/*
 * 
 * This is jemula.
 *
 *    Copyright (c) 2006-2009 Stefan Mangold, Fabian Dreier, Stefan Schmid
 *    All rights reserved. Urheberrechtlich geschuetzt.
 *    
 *    Redistribution and use in source and binary forms, with or without modification,
 *    are permitted provided that the following conditions are met:
 *    
 *      Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer. 
 *    
 *      Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation and/or
 *      other materials provided with the distribution. 
 *    
 *      Neither the name of any affiliation of Stefan Mangold nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission. 
 *    
 *    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *    OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *    IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 *    OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 *    OF SUCH DAMAGE.
 *    
 */

package statistics;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import kernel.JEmula;

/**
 * @author Stefan Mangold
 */
public abstract class JEStatEval extends JEmula {

    private Path thePath2File;

	private Map<String,JEStatsOutput> outWriters;

	protected ArrayList<String> output;

	protected List<Number> theSampleList1;

	protected List<Number> theSampleList2;

	protected List<Number> theSampleList3;

	protected Vector<Number> theEvalList1;

	protected Vector<Number> theEvalList2;

	protected Vector<Number> theEvalList3;

	protected Vector<Number> theEvalList4;

	protected Vector<Number> theEvalList5;

	protected Vector<Number> theEvalList6;

	protected Vector<Number> theEvalList7;

	protected Vector<Number> theEvalList8;

	protected int theSum1;

	protected double theSum2;

	protected double theSum3;
	
	protected Map<Long,Object> sampleMap = new HashMap<>();

	protected JEStatEval(String aPath, String aFileName, String[] aHeaderLine) {

		this.thePath2File = Paths.get(aPath,aFileName);

        this.theSampleList1 = new ArrayList<>();
        this.theSampleList2 = new ArrayList<>();
        this.theSampleList3 = new ArrayList<>();
        this.theEvalList1 = new Vector<>();
        this.theEvalList2 = new Vector<>();
        this.theEvalList3 = new Vector<>();
        this.theEvalList4 = new Vector<>();
        this.theEvalList5 = new Vector<>();
        this.theEvalList6 = new Vector<>();
        this.theEvalList7 = new Vector<>();
        this.theEvalList8 = new Vector<>();
        this.theSum1 = 0;
        this.theSum2 = 0.0;
        this.theSum3 = 0.0;

        this.reset();

        this.outWriters = new HashMap<>();
        Path mFile = Paths.get(aPath, aFileName+".m");
		Path csvFile = Paths.get(aPath, aFileName+".csv");
		this.outWriters.putIfAbsent("m",new JEStatsOutput(mFile, makeHeaderForMatlabFormat(aHeaderLine)));
		this.outWriters.putIfAbsent("csv",new JEStatsOutput(csvFile, makeHeaderForCSVFormat(aHeaderLine)));
    }

    private String makeHeaderForMatlabFormat(String[] aHeaderLine) {

		List<String> numberedHeaderLine = new ArrayList<>(aHeaderLine.length);
		int i = 0;
		for(String header: aHeaderLine) {
			numberedHeaderLine.add(String.format("(%d) %s", i++, header));
		}
        String header = "%=============================================================================================================================\n" +
                "% Result \"" + this.toString() + "\". " + new Date().toString() + ".\n" +
                "%-----------------------------------------------------------------------------------------------------------------------------\n" +
                "% " + String.join(" | ", numberedHeaderLine) +
                "\n%=============================================================================================================================\n" +
                "result_" + this.toString() + " = [";
        return header;
	}

	private String makeHeaderForCSVFormat(String[] aHeaderLine) {
		return String.join(",", aHeaderLine);
	}

	public boolean sampleNoDuplicate(double aTime_ms, int aValue1, long aValue2, double aValue3) {
		boolean toSample = (sampleMap.get(aValue2) == null);
		if(toSample) {
			sample(aTime_ms, aValue1, aValue2, aValue3);
		}
		return toSample;
	}

	public void sample(double aTime_ms, int aValue1, long aValue2, double aValue3) {
		sampleMap.put(aValue2, new Object());
		this.theSampleList1.add(aValue1);
		this.theSampleList2.add(aValue2);
		this.theSampleList3.add(aValue3);
	}

	public void reset() {
		this.theEvalList1.clear();
		this.theEvalList2.clear();
		this.theEvalList3.clear();
		this.theEvalList4.clear();
		this.theEvalList5.clear();
		this.theEvalList6.clear();
		this.theEvalList7.clear();
		this.theEvalList8.clear();

		this.theSum1 = 0;
		this.theSum2 = 0.0;
		this.theSum3 = 0.0;

		this.theSampleList1.clear();
		this.theSampleList2.clear();
		this.theSampleList3.clear();
	}

	public void end_of_emulation() {
		JEStatsOutput out = outWriters.get("m");
		out.write("]; % " + new Date().toString());
	}
	
	public Vector<Number> getEvalList1() {
		return theEvalList1;
	}

	public Vector<Number> getEvalList2() {
		return theEvalList2;
	}
	
	public Vector<Number> getEvalList3() {
		return theEvalList3;
	}
	
	public Vector<Number> getEvalList4() {
		return theEvalList4;
	}

	public Vector<Number> getEvalList5() {
		return theEvalList5;
	}
	
	public Vector<Number> getEvalList6() {
		return theEvalList6;
	}
	
	public Vector<Number> getEvalList7() {
		return theEvalList7;
	}
	
	public Vector<Number> getEvalList8() {
		return theEvalList8;
	}
	
	@Override
	public String toString() {
        String fileName = this.thePath2File.getFileName().toString();
        if (fileName.indexOf(".") > 0) {
            fileName = fileName.substring(0, fileName.lastIndexOf("."));
        }
		return fileName;
	}

    protected void write(){
		for(Map.Entry<String, JEStatsOutput> entry : outWriters.entrySet()){
			JEStatsOutput out = entry.getValue();
			out.write(this.output);
		}
	}
}
