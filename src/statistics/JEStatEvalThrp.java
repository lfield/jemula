/*
 * 
 * This is jemula.
 *
 *    Copyright (c) 2006-2009 Stefan Mangold, Fabian Dreier, Stefan Schmid
 *    All rights reserved. Urheberrechtlich geschuetzt.
 *    
 *    Redistribution and use in source and binary forms, with or without modification,
 *    are permitted provided that the following conditions are met:
 *    
 *      Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer. 
 *    
 *      Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation and/or
 *      other materials provided with the distribution. 
 *    
 *      Neither the name of any affiliation of Stefan Mangold nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission. 
 *    
 *    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *    OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *    IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 *    OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 *    OF SUCH DAMAGE.
 *    
 */

package statistics;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Stefan Mangold
 *
 */
public class JEStatEvalThrp extends JEStatEval {

	/**
	 *
	 */
	public JEStatEvalThrp(String aPath, String aFileName) {
		this(aPath, aFileName, getDefaultHeaderLine(aFileName.toLowerCase()));
	}
	public JEStatEvalThrp(String aPath, String aFileName, String[] aHeaderLine) {
		super(aPath, aFileName, aHeaderLine);
		this.output = new ArrayList<>(7); // Each output line contains 7 strings
		this.message(this.getClass().getSimpleName() + " constructor done.", 0);
	}

	private static String[] getDefaultHeaderLine(String aFileName) {
		if(aFileName.contains("offer")) {
			return new String[]{"time[ms]", "#packets last interval", "#packets overall", "overall avrg. packetsize [byte]", "overall sum packetsize [byte]", "overall offer [Mb/s]", "offer last interval [Mb/s]"};
		} else {
			return new String[]{"time[ms]", "#packets last interval", "#packets overall", "overall avrg. packetsize [byte]", "overall sum packetsize [byte]", "overall thrp [Mb/s]", "thrp last interval [Mb/s]"};
		}
	}

	public void evaluation(double anEvalTime_ms) {

		int aNumberOfSamples = this.theSampleList1.size();

		this.theSum1 = this.theSum1 + aNumberOfSamples;


		double anAverage1 = 0.0;
		int cnt = 0;
		for (cnt = 0; cnt < aNumberOfSamples; cnt++) {
			if (this.theSampleList1.get(cnt) != null) {
				anAverage1 = anAverage1 + (Integer) this.theSampleList1.get(cnt);
			}
		}
		anAverage1 = anAverage1 / cnt;

		double anAverage2 = 0.0;
		for (cnt = 0; cnt < aNumberOfSamples; cnt++) {
			if (this.theSampleList2.get(cnt) != null) {
				anAverage2 = anAverage2 + (Long) this.theSampleList2.get(cnt);
			}
		}
		anAverage2 = anAverage2 / cnt;

		for (cnt = 0; cnt < aNumberOfSamples; cnt++) {
			if (this.theSampleList2.get(cnt) != null) {
				this.theSum2 = this.theSum2 + (Long) this.theSampleList2.get(cnt);
			}
		}

		double anAverage3 = 0.0;
		for (cnt = 0; cnt < this.theSampleList3.size(); cnt++) {
			if (this.theSampleList3.get(cnt) != null) {
				anAverage3 = anAverage3 + (Double) this.theSampleList3.get(cnt);
			}
		}
		anAverage3 = anAverage3 / cnt;

		for (cnt = 0; cnt < this.theSampleList3.size(); cnt++) {
			if (this.theSampleList3.get(cnt) != null) {
				this.theSum3 = this.theSum3 + (Double) this.theSampleList3.get(cnt);
			}
		}

		double aTotalEvalTime_ms = 0.0;
		if (this.theEvalList1.size() > 0) {
			aTotalEvalTime_ms = anEvalTime_ms - (Double) this.theEvalList1.get(0);
		} 

		double aTotalMbps = this.theSum3 / aTotalEvalTime_ms * 1000 * 8 / 1e6;

		double intervall;
		if(!theEvalList1.isEmpty()){
			intervall = anEvalTime_ms-(Double)theEvalList1.get(theEvalList1.size()-1);
		} else {
			intervall = 0;
		}
		
		this.theEvalList1.add(anEvalTime_ms);

//		if (this.theSampleList1.size() > 0) {
//			this.theEvalList2.add(this.theSampleList1.get(this.theSampleList1.size() - 1));
//		} else {
//			this.theEvalList2.add(Double.NaN);
//		}
		
		
		this.theEvalList3.add(aNumberOfSamples);
		this.theEvalList4.add(this.theSum1);
		this.theEvalList5.add(anAverage3);
		this.theEvalList6.add(this.theSum3);
		this.theEvalList7.add(aTotalMbps);
		double bytes = aNumberOfSamples*anAverage3;
		double mBit = bytes/1e6*8;
		double factor = (1/(intervall/1000));
		double currentTP = mBit*factor;

		writeOutput(currentTP);

		this.theSampleList1.clear();
		this.theSampleList2.clear();
		this.theSampleList3.clear();
		super.sampleMap = new HashMap<>();
	}

	private void writeOutput(double currentTP){
		//Clear old output
		this.output.clear();

		this.output.add(this.theEvalList1.lastElement().toString());	// time[ms]
		this.output.add(this.theEvalList3.lastElement().toString());	// #packets
		this.output.add(this.theEvalList4.lastElement().toString());	// overall #packets
		this.output.add(this.theEvalList5.lastElement().toString());	// avrg.packetsize[byte]
		this.output.add(this.theEvalList6.lastElement().toString());	// overall sum packetsize[byte]
		this.output.add(this.theEvalList7.lastElement().toString());	// thrpt overall[Mb/s]
		this.output.add(Double.toString(currentTP));			// thrpt last interval[Mb/s]

		super.write();
	}

}
