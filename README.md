This is Jemula++
================

Jemula is a kernel for event-driven stochastic simulation. Since it is prepared to simulate real-time systems, we prefer to refer to it as an "emulator" instead of just a plain simulator. Jemula is used in the [Jemula802++](https://smangold@bitbucket.org/lfield/jemula802.git) project.

The tool is published as is under a BSD free software license. It is primarily used for teaching and maintained by Lovefield Wireless.

  * [Class "Wireless and Mobile Computing for Entertainment Applications" at ETH Zurich](http://www.lst.inf.ethz.ch/education/wireless2020.html)
  * [Lovefield Wireless GmbH](http://www.lovefield.ch)

![Jemula802](https://bytebucket.org/lfield/jemula802/raw/75eaa2569bbc7f9add32fe720eb5434cb917be73/resources/MontanaDirectionalAntennas/1.jpg "Jemula802")
![Jemula802](https://bytebucket.org/lfield/jemula802/raw/75eaa2569bbc7f9add32fe720eb5434cb917be73/resources/MontanaDirectionalAntennas/2.jpg "Jemula802")
![Jemula802](https://bytebucket.org/lfield/jemula802/raw/75eaa2569bbc7f9add32fe720eb5434cb917be73/resources/MontanaDirectionalAntennas/3.jpg "Jemula802")
![Jemula802](https://bytebucket.org/lfield/jemula802/raw/75eaa2569bbc7f9add32fe720eb5434cb917be73/resources/MontanaDirectionalAntennas/4.jpg "Jemula802")
![Jemula802](https://bytebucket.org/lfield/jemula802/raw/75eaa2569bbc7f9add32fe720eb5434cb917be73/resources/MontanaDirectionalAntennas/5.jpg "Jemula802")
![Jemula802](https://bytebucket.org/lfield/jemula802/raw/75eaa2569bbc7f9add32fe720eb5434cb917be73/resources/MontanaDirectionalAntennas/6.jpg "Jemula802")
![Jemula802](https://bytebucket.org/lfield/jemula802/raw/75eaa2569bbc7f9add32fe720eb5434cb917be73/resources/MontanaDirectionalAntennas/7.jpg "Jemula802")
![Jemula802](https://bytebucket.org/lfield/jemula802/raw/75eaa2569bbc7f9add32fe720eb5434cb917be73/resources/MontanaDirectionalAntennas/8.jpg "Jemula802")
![Jemula802](https://bytebucket.org/lfield/jemula802/raw/75eaa2569bbc7f9add32fe720eb5434cb917be73/resources/MontanaDirectionalAntennas/9.jpg "Jemula802")
![Jemula802](https://bytebucket.org/lfield/jemula802/raw/75eaa2569bbc7f9add32fe720eb5434cb917be73/resources/MontanaDirectionalAntennas/10.jpg "Jemula802")
